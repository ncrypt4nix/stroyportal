# coding=utf-8
import os
import json
import time
import requests
import pandas as pd
from progress.bar import Bar
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from collections import defaultdict
from optparse import make_option
from django.db import IntegrityError
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from transliterate import translit
from common.utils.elastic_prepare import normalize_phrase
from apps.models.models import (
    ProductSearchPhrase,
    Section,
    ProductSearchPhraseType,
)


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option(
            '--save',
            action='store_true',
            dest='save',
            default=False,
            help='Save tags to database'
        ),
        make_option(
            '--tag_file',
            action='store',
            type='string',
            dest='tag_file',
            default='',
            help='Path to file with tags'
        ),
        make_option(
            '--basis',
            action='store',
            type='string',
            dest='basis',
            default='',
            help='Basis name'
        ),
        make_option(
            '--section_id',
            action='store',
            type='int',
            dest='section_id',
            default=0,
            help='Section where to create tags'
        ),
    )
    ELASTIC_HOST = settings.ELASTIC_URL.rstrip('/')
    MIN_SEARCH_QUERY = 10
    YANDEX_PASSPORT_URL = 'https://passport.yandex.ru/passport?mode=auth&msg=&retpath=https%3A%2F%2Fwordstat.yandex.ru%2F'

    def phrase_analizer(self, phrase):
        """Анализирует фразу на теги запросом к Elasticsearch"""
        response = requests.get(
            '%s/tag/_search' % self.ELASTIC_HOST,
            data=json.dumps({
                "query": {
                    "bool": {
                        "must": [
                            {"match": {"tag": translit(phrase, 'ru')}},
                            {"match_phrase": {"basis": self.BASIS}}
                        ]
                    }
                }
            }),
            headers={'Content-Type': 'application/json'}
        ).json()
        response['hits']['hits'].sort(key=lambda i: i['_score'])
        return {
            i['_source']['type']: i['_source']['tag']
            for i in response['hits']['hits']
        }

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.driver = webdriver.Firefox()
        self.driver.get(self.YANDEX_PASSPORT_URL)
        login_input = self.driver.find_element_by_id('passp-field-login')
        login_input.send_keys(settings.YANDEX_SPAM_LOGIN + Keys.ENTER)
        password_input = WebDriverWait(self.driver, 6).until(
            EC.presence_of_element_located((By.ID, 'passp-field-passwd'))
        )
        password_input.send_keys(settings.YANDEX_SPAM_PASSWORD + Keys.ENTER)

    def correct_word_order(self, phrase):
        phrase = phrase.lower()
        text_input = self.driver.find_element_by_name('text')
        try:
            text_input.send_keys(phrase)
            text_input.send_keys(Keys.ENTER)
            time.sleep(2) #  give the driver time to erase the element below
            text = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'b-phrase-link__link'))
            ).text
        except TimeoutException:
            return phrase
        finally:
            text_input.clear()
        return text

    def handle(self, *args, **options):
        from apps.auto_link.models import KeywordFrequency

        if not options['tag_file'] or not options['basis'] or not options['section_id']:
            raise CommandError('Options tag_file, section_id and basis required')
        if not os.path.isfile(options['tag_file']):
            raise CommandError('File does not exist')
        TAG_FILEPATH = options['tag_file']
        self.BASIS = options['basis']
        BASE_DB_TAG_NAME = normalize_phrase(self.BASIS, with_delay=True).pop()

        tags_df = pd.read_csv(TAG_FILEPATH).dropna(
            axis='columns',
            how='all'
        )
        tags_df = tags_df.drop([i for i in tags_df.columns if 'Unnamed' in i], axis=1)

        # Переименовываем колонки по уже существующим типам фраз
        phrase_types = {
            i.title: normalize_phrase(i.title, with_delay=True)
            for i in ProductSearchPhraseType.objects.all()
        }
        rename_tags = {}
        with Bar('Prepare file:', max=len(phrase_types), suffix='%(percent)d%%') as bar:
            for title, token_set in bar.iter(phrase_types.items()):
                for column in tags_df.columns:
                    if normalize_phrase(column, with_delay=True) == token_set:
                        rename_tags[column] = title
        tags_df = tags_df.rename(columns=rename_tags).transpose()

        print 'Query to KeywordFrequency...'
        # Загружаем базу запросов
        query = KeywordFrequency.objects.filter(
            phrase__title__icontains=BASE_DB_TAG_NAME,
            exact_frequency__gt=self.MIN_SEARCH_QUERY
        )
        df = pd.DataFrame(query.values(
            'exact_frequency',
            'phrase__title'
        )).fillna(0).sort_values(
            by=['exact_frequency']
        )

        # Ранжируем группы
        # Парсинг запросов на термы и их нормализация.
        with Bar('Parse keyword frequency:', max=len(df), suffix='%(percent)d%%') as bar:
            term_count = defaultdict(int)
            for item, row in bar.iter(df.iterrows()):
                for token in normalize_phrase(row['phrase__title'], with_delay=True):
                    term_count[token] += row['exact_frequency']
        # Нормализуем данные и вычислим веса
        with Bar('Arranging weights:', max=len(tags_df), suffix='%(percent)d%%') as bar:
            groups_range = defaultdict(int)
            for group_name, group_row in bar.iter(tags_df.iterrows()):
                for phrase in group_row.dropna():
                    for token in normalize_phrase(phrase, with_delay=True):
                        groups_range[group_name] += term_count[token]
        # Результат в DataFrame для анализа
        groups_df = pd.DataFrame(
            groups_range.items(),
            columns=['group', 'weight']
        ).sort_values(by=['weight'])
        # Отрежем колонки по среднему
        columns = [
            unicode(i) for i in groups_df[
                groups_df.mean().weight < groups_df.weight
            ]['group']
        ]
        columns.reverse()
        _tags_df = tags_df.transpose()
        tags_df = _tags_df.drop(
            [i for i in _tags_df.columns if i not in columns],
            axis=1
        ).transpose()
        with Bar('Prepare Elasticsearch:', max=len(tags_df), suffix='%(percent)d%%') as bar:
            #  пересоздадим индекс в elasticsearch
            requests.delete('%s/tag/' % self.ELASTIC_HOST)
            requests.put(
                '%s/tag/' % self.ELASTIC_HOST,
                data=json.dumps({
                    "mappings": {
                        "properties": {
                            "basis": {
                                "type": "text",
                                "analyzer": "russian"
                            },
                            "type": {
                                "type": "text",
                                "analyzer": "russian"
                            },
                            "tag": {
                                "type": "text",
                                "analyzer": "russian"
                            }
                        }
                    }
                }),
                headers={'Content-Type': 'application/json'}
            )
            # Создаем теги в elasticsearch
            for typename, row in bar.iter(tags_df.iterrows()):
                for tagname in row.dropna():
                    requests.post(
                        '%s/tag/_doc' % self.ELASTIC_HOST,
                        data=json.dumps({
                            "basis": self.BASIS,
                            "type": typename,
                            "tag": tagname
                        }),
                        headers={'Content-Type': 'application/json'}
                    )

        print 'Processing data...'
        df['type_list'] = df.apply(
            lambda row: self.phrase_analizer(row['phrase__title']),
            axis=1
        )
        # Сохраняем список нерелевантных фраз в файл, для анализа человеком
        df[0 == df.type_list.map(len)].to_csv(
            'not_relevant.csv',
            columns=['phrase__title']
        )
        # Удаляем спам
        df = df[df.type_list.map(len) != 0]
        # Раскрываем словари и убираем изначальные фразы
        df = pd.concat(
            [
                df.drop(['type_list', 'phrase__title'], axis=1),
                df['type_list'].apply(pd.Series)
            ],
            axis=1
        )
        # Суммируем дупликаты
        df = df.fillna('').groupby(
            [i for i in df.columns if i != 'exact_frequency']
        )['exact_frequency'].sum().reset_index().sort_values(
            by=['exact_frequency'],
        )

        # Нормализуем уже существующие теги
        section = Section.objects.get(id=options['section_id'])
        s_phrases = ProductSearchPhrase.objects.filter(section=section)
        s_phrases_token = {
            s_phrase: normalize_phrase(s_phrase.phrase, with_delay=True)
            for s_phrase in s_phrases
        }
        # Отсортируем DataFrame по релевантности
        df = df[columns + ['exact_frequency']]
        # Создадим новые типы фраз
        phrase_types = {}
        for column in columns:
            phrase_types[column], _ = ProductSearchPhraseType.objects.get_or_create(
                title=column
            )
        if not options['save']:
            print 'Created file: created_tags.csv'
            df.to_csv('created_tags.csv')
            return
        # Находим пересечения с уже существующими тегами
        # И создаем новые теги
        with Bar('Create tags:', max=len(df), suffix='%(percent)d%%') as bar:
            phrase_tree = {}
            for num, row in bar.iter(df.iloc[::-1].iterrows()):
                parent = None
                phrase = self.BASIS
                for column in columns:
                    if not row[column]:
                        continue
                    phrase_value = row[column]
                    phrase_n = normalize_phrase(phrase, with_delay=True)
                    dupl_flag = True
                    for t in normalize_phrase(phrase_value, with_delay=True):
                        if t not in phrase_n:
                            dupl_flag = False
                            break
                    if dupl_flag:
                        continue
                    phrase = ' '.join([phrase, phrase_value])
                    # Попробуем найти такую уже существующую фразу
                    phrase_n = normalize_phrase(phrase, with_delay=True)
                    flag_phrase = False
                    for s_phrase, token_set in s_phrases_token.items():
                        if set(phrase_n) == token_set:
                            flag_phrase = s_phrase
                            break
                    if phrase_tree.get(phrase):
                        obj = phrase_tree[phrase]
                    else:
                        phrase = self.correct_word_order(phrase.strip().lower())
                        obj = (
                            flag_phrase
                            if flag_phrase
                            else ProductSearchPhrase.objects.get_or_create(
                                phrase=phrase,
                                section=section
                            )[0]
                        )
                        defaults = {
                            'section': section,
                            'parent':  parent,
                            'phrase':  phrase,
                            'phrase_type':  phrase_types[column],
                            'weight':  row['exact_frequency'],
                            'tagname':  phrase_value,
                            'moderated':  False,
                        }
                        for key, value in defaults.items():
                            setattr(obj, key, value)
                        try:
                            obj.save()
                        except IntegrityError:
                            # Перезаписываем изменения, если уже есть в базе
                            obj = ProductSearchPhrase.objects.get(phrase=phrase)
                            for key, value in defaults.items():
                                setattr(obj, key, value)
                            obj.save()
                        phrase_tree[phrase] = obj
                    parent = obj
