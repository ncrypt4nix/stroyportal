# coding=utf-8
import os
import re
import abc
import time
import threading
import urllib
import random
from progress.bar import Bar
from collections import namedtuple, defaultdict
from requests.exceptions import SSLError, ProxyError, ConnectionError
from urlparse import urlparse
from bs4 import BeautifulSoup
from django.core.files import File
from django.conf import settings
from raven.transport import requests
from apps.models.models import (
    Characteristic,
    ProductCharacteristic,
    ProductModelImage,
    ProductModelVideo,
    ProductModelDocument,
    TempProductCharacteristic
)


class ParseFabricMetaAbstract(abc.ABCMeta):
    inheritors = []

    def __new__(cls, name, bases, dct):
        _class = super(ParseFabricMetaAbstract, cls).__new__(cls, name, bases, dct)
        if not _class.__abstractmethods__:
            cls.inheritors.append(_class)
        return _class


def parser_fabric(model, controller):
    for inheritor in ParseFabricMetaAbstract.inheritors:
        if model.origin_product.other_shop and inheritor.get_hostname() in model.origin_product.other_shop.lower():
            return inheritor(model, controller)


class ParseController:
    class ControllerBar(Bar):
        def __init__(self, show=False, *args, **kwargs):
            self.show = show
            if self.show:
                super(ParseController.ControllerBar, self).__init__(*args, **kwargs)
            else:
                self.next = lambda _: _
                self.finish = lambda _: _

    def __init__(self, models, with_bar=False, print_exc=False):
        self.with_bar = with_bar
        self.print_exc = print_exc
        self.models = models

        self.to_bulk_chars = []
        self.to_bulk_images = []
        self.to_bulk_videos = []
        self.to_bulk_docs = []

        model_ids = [i.id for i in self.models]
        # prepare images
        model_images = ProductModelImage.objects.exclude(
            external_url=None
        ).only(
            'external_url',
            'image',
            'model'
        )
        self.existed_images = {
            i.external_url: i.image
            for i in model_images
        }
        self.model_existed_images = defaultdict(list)
        for image in model_images:
            if image.model_id not in model_ids:
                continue
            self.model_existed_images[image.model_id].append(image.external_url)
        # prepare documents
        model_documents = ProductModelDocument.objects.exclude(
            external_url=None
        ).only(
            'external_url',
            'document',
            'model'
        )
        self.existed_docs = {
            i.external_url: i.document
            for i in model_documents
        }
        self.model_existed_documents = defaultdict(list)
        for document in model_documents:
            if document.model_id not in model_ids:
                continue
            self.model_existed_documents[document.model_id].append(document.external_url)
        # prepare videos
        self.model_existed_videos = defaultdict(list)
        for video in ProductModelVideo.objects.filter(model__in=self.models).only('model', 'video_id'):
            self.model_existed_videos[video.model_id].append(video.video_id)
        # prepare chars
        self.model_existed_chars = defaultdict(list)
        char_qs = ProductCharacteristic.objects.filter(
            product_model__in=self.models
        ).select_related(
            'characteristic'
        ).only('product_model', 'characteristic__group_name')
        for product_char in char_qs:
            self.model_existed_chars[product_char.product_model_id].append(
                product_char.characteristic.group_name
            )

    def _save_data(func):
        """save data on error"""
        def wrap(self, save_data=True, *args, **kwargs):
            if not save_data:
                return func(self, *args, **kwargs)
            try:
                return func(self, *args, **kwargs)
            except Exception as exc:
                print '\n', 'Exception: ', exc
            finally:
                self.bulk_save()
        return wrap

    @_save_data
    def parse(self):
        def extend_data(list_, func):
            list_.extend(func())

        bar = self.ControllerBar(message='Download files: ', max=len(self.models), show=self.with_bar)
        for model in self.models:
            bar.next()
            try:
                parser = parser_fabric(model, self)
                if not parser:
                    continue
            except (ConnectionError, AttributeError):
                continue
            wait_description = threading.Thread(target=parser.parse_description)
            wait_chars = threading.Thread(target=extend_data, args=(self.to_bulk_chars, parser.parse_chars))
            wait_images = threading.Thread(target=extend_data, args=(self.to_bulk_images, parser.parse_images))
            wait_videous = threading.Thread(target=extend_data, args=(self.to_bulk_videos, parser.parse_videos))
            wait_docs = threading.Thread(target=extend_data, args=(self.to_bulk_docs, parser.parse_docs))
            threads = [wait_description, wait_chars, wait_images, wait_videous, wait_docs]
            for thread in threads:
                thread.start()
            for thread in threads:
                thread.join()
        bar.finish()

    def bulk_save(self):
        bar = self.ControllerBar(message='Save downloaded files: ', max=4, show=self.with_bar)
        ProductCharacteristic.objects.bulk_create(self.to_bulk_chars)
        bar.next()
        ProductModelImage.objects.bulk_create(self.to_bulk_images)
        bar.next()
        ProductModelVideo.objects.bulk_create(self.to_bulk_videos)
        bar.next()
        ProductModelDocument.objects.bulk_create(self.to_bulk_docs)
        bar.next()
        bar.finish()

    def clear(self):
        del self.models
        del self.to_bulk_chars
        del self.to_bulk_images
        del self.to_bulk_videos
        del self.to_bulk_docs


class ModelParseStrategy:
    __metaclass__ = ParseFabricMetaAbstract
    Doc = namedtuple('Doc', ['href', 'title'])

    domain = property(abc.abstractmethod(lambda _: _))
    brand_phrase = ''
    no_image_url = None
    # Переопределите URL'ы у конкретной стратегии
    response_router = {
        'description': None,
        'video': None,
        'image': None,
        'char': None,
        'doc': None,
    }
    controller = None

    def __init__(self, model, controller):
        self.model = model
        self.product = model.origin_product
        self.domain = self.domain.rstrip('/') + '/'
        self.product.other_shop = self.product.other_shop.lower()
        # Вырезаем url из параметров другого url'а, если нужно
        self.product.other_shop = next(
            '://'.join([self.get_http_protocol(), link])
            for link in re.split(r'https?://', self.product.other_shop)
            if self.get_hostname() in link
        )
        # Убирает get параметры у продукта
        self.product.other_shop = self.product.other_shop.split('?')[0].lower()
        # Нужна для метода _get_response
        self._response = self.__get_response()
        self.controller = controller

    def _get_response(self, urlname):
        return (
            self.__get_response(self.response_router[urlname])
            if self.response_router.get(urlname)
            else self._response
        )

    def __get_response(self, suffix=''):
        def response_timeout(url):
            timeout = 0.3
            attempts = 5
            for i in xrange(attempts):
                try:
                    if not getattr(settings, 'PROXIES', None):
                        return requests.get(url)

                    proxy_index = random.randint(0, len(settings.PROXIES) - 1)
                    return requests.get(
                        url,
                        proxies={
                            "http": settings.PROXIES[proxy_index],
                            "https": settings.PROXIES[proxy_index]
                        },
                        headers={
                            'User-Agent': 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)'
                        }
                    )
                except (SSLError, ProxyError):
                    time.sleep(timeout)
                    timeout += random.uniform(0.1, timeout)
            raise ConnectionError

        url = '/'.join([self.product.other_shop.rstrip('/'), suffix.lstrip('/')])
        response = response_timeout(url)
        if response.url != url:
            url = '/'.join([response.url, suffix.lstrip('/')])
            response = response_timeout(url)
        return response

    def _cache_method(func):
        def wrap(cls, *args, **kwargs):
            key = 'func_{func}_cached'.format(func=func.__name__)
            if not hasattr(cls, key):
                setattr(cls, key, func(cls, *args, **kwargs))
            return getattr(cls, key)
        return wrap

    @classmethod
    @_cache_method
    def get_hostname(cls):
        return re.sub(r'(https?://)?(www.)?', '', cls.domain).lower().rstrip('/')

    @classmethod
    @_cache_method
    def get_http_protocol(cls):
        try:
            return re.search(r'^https?', cls.domain).group(0)
        except AttributeError:
            return 'http'

    @staticmethod
    def get_path_and_download_file(func, src):
        path = func(os.path.basename(src))
        dir_path = os.path.join(settings.MEDIA_ROOT, os.path.dirname(path))
        if not os.path.isdir(dir_path):
            try:
                os.makedirs(dir_path)
            except OSError:  # network problem
                pass
        full_path = os.path.join(settings.MEDIA_ROOT, path)
        with open(full_path, 'wb') as out:
            out.write(requests.get(src).content)
            os.chmod(full_path, 0775)
        return path

    @abc.abstractmethod
    def find_unit(self, soup):
        pass

    @abc.abstractmethod
    def find_description(self, soup):
        """Ожидается возвращение BeautifulSoup обьекта с коренем DOM-дерева, которое не попадет в БД"""
        pass

    @abc.abstractmethod
    def find_videos(self, soup):
        """Ожидается возвращение списка iframe'ов Google или диктов с ключом 'src' или 'href'"""
        pass

    @abc.abstractmethod
    def find_images(self, soup):
        """Ожидается возвращение списка диктов с ключом src"""
        pass

    @abc.abstractmethod
    def find_chars(self, soup):
        """Ожидается возвращение списка кортежей вида: (название, значение)"""
        pass

    @abc.abstractmethod
    def find_docs(self, soup):
        """Ожидается возвращение списка обьектов ModelParseStrategy.Doc"""
        pass

    @staticmethod
    def str_from_html(html):
        return html.replace('\n\n', '\n').strip('\n').strip()

    def parse_description(self):
        response = self._get_response('description')
        soup = BeautifulSoup(response.content, 'lxml')
        try:
            description_soup = self.find_description(soup)
            description = ''.join(
                map(
                    str,
                    description_soup.findChildren(recursive=False)
                )
            ).replace(self.brand_phrase, '')
            noodles = sum([
                description_soup.findAll('style'),
                description_soup.findAll('script'),
                description_soup.findAll('img'),
                description_soup.findAll('iframe')
            ], [])
            for tag in noodles:
                description = description.replace(str(tag), '')
            description = self.str_from_html(
                norm_description(
                    description
                )
            )
            if description == self.model.description:
                return
            self.model.description = description
            self.model.save()
        except AttributeError as e:
            if self.controller.print_exc:
                print 'parse_description: {}'.format(str(e))

    def parse_chars(self):
        UNIT_NAME = u'Единицы измерения производителя'
        response = self._get_response('char')
        soup = BeautifulSoup(response.content, 'lxml')
        chars = []
        try:
            unit = self.find_unit(soup)
            if unit and UNIT_NAME not in self.controller.model_existed_chars[self.model.id]:
                char = Characteristic(
                    group_name=UNIT_NAME,
                    value=self.str_from_html(unit)
                )
                char.save()
                chars.append(ProductCharacteristic(product_model=self.model, characteristic=char))
        except AttributeError:
            pass
        try:
            char_length = Characteristic._meta.get_field_by_name('group_name')[0].max_length
            for char_line in self.find_chars(soup):
                if not char_line or len(char_line) < 2:
                    continue
                group_name = self.str_from_html(char_line[0])[:char_length]
                # Удалить знаки вопроса из html характеристики
                group_name = group_name.strip('?').strip()
                if group_name in self.controller.model_existed_chars[self.model.id]:
                    continue
                else:
                    self.controller.model_existed_chars[self.model.id].append(group_name)
                char = Characteristic(
                    group_name=group_name,
                    value=self.str_from_html(char_line[1]),
                )
                char.save()
                chars.append(ProductCharacteristic(product_model=self.model, characteristic=char))
        except AttributeError as e:
            if self.controller.print_exc:
                print 'parse_chars: {}'.format(str(e))
        return chars

    def parse_images(self):
        images = []
        response = self._get_response('image')
        soup = BeautifulSoup(response.content, 'lxml')
        try:
            external_url_len = ProductModelImage._meta.get_field_by_name('external_url')[0].max_length
            for img in self.find_images(soup)[::-1]:
                src = img.get('src') or img.get('data-lazy')
                if not src or src == self.no_image_url:
                    continue
                if src.startswith('/'):
                    src = (
                        ':'.join([self.get_http_protocol(), src])
                        if src.startswith('//')
                        else self.domain + src
                    )
                if src in self.controller.model_existed_images[self.model.id]:
                    continue
                image = None
                if src in self.controller.existed_images:
                    image = self.controller.existed_images[src]
                else:
                    image = self.get_path_and_download_file(
                        ProductModelImage(model=self.model).get_product_image_storage,
                        src
                    )
                    self.controller.existed_images[src] = image
                images.append(
                    ProductModelImage(
                        model=self.model,
                        image=image,
                        external_url=src[:external_url_len],
                    )
                )
        except AttributeError as e:
            if self.controller.print_exc:
                print 'parse_images: {}'.format(str(e))
        return images

    def parse_videos(self):
        videos = []
        response = self._get_response('video')
        soup = BeautifulSoup(response.content, 'lxml')
        try:
            for frame in self.find_videos(soup):
                src = frame.get('src') or frame.get('href')
                if not src:
                    continue
                if src.startswith('/'):
                    src = self.domain + src
                if not src.__contains__('embed'):
                    continue
                video_id = src.split('/')[-1]
                if video_id in self.controller.model_existed_videos[self.model.id]:
                    continue
                videos.append(
                    ProductModelVideo(
                        model=self.model,
                        video_id=video_id,
                    )
                )
        except AttributeError as e:
            if self.controller.print_exc:
                print 'parse_videos: {}'.format(str(e))
        return videos

    def parse_docs(self):
        response = self._get_response('doc')
        soup = BeautifulSoup(response.content, 'lxml')
        docs = []
        try:
            external_url_len = ProductModelDocument._meta.get_field_by_name('external_url')[0].max_length
            title_len = ProductModelDocument._meta.get_field_by_name('title')[0].max_length
            for doc in self.find_docs(soup):
                href = doc.href
                if href.startswith('/'):
                    href = self.domain + href
                if href in self.controller.model_existed_documents[self.model.id]:
                    continue
                document = None
                if href in self.controller.existed_docs:
                    document = self.controller.existed_docs[href]
                else:
                    document = self.get_path_and_download_file(
                        ProductModelDocument(model=self.model).model_file_storage,
                        href
                    )
                    self.controller.existed_docs[href] = document
                docs.append(
                    ProductModelDocument(
                        model=self.model,
                        title=doc.title[:title_len],
                        document=document,
                        external_url=href[:external_url_len],
                    )
                )
        except AttributeError as e:
            if self.controller.print_exc:
                print 'parse_docs: {}'.format(str(e))
        return docs


class ConcreteTSKrovIzol(ModelParseStrategy):
    domain = 'https://ts-krovizol.ru/'
    no_image_url = '/local/templates/ts_krovizol/img/no_photo.png'

    def find_images(self, soup):
        images = []
        for image in soup.find(id='good_gallery').findAll('img'):
            if not image['src'].startswith('/upload'):
                continue
            image['src'] = re.sub(r'(resize_cache/)?(70_50_2/)?', '', image['src'])
            if image not in images:
                images.append(image)
        return images

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        return soup.find(id='description').find(class_='section-body')

    def find_chars(self, soup):
        return [
            (i.find('dt').text, i.find('dd').text)
            for i in soup.find(id='characteristics').findAll('dl')
        ]

    def find_docs(self, soup):
        return [
            self.Doc(
                href=i.find('dd').find('a')['href'],
                title=i.find('dt').text
            )
            for i in soup.find(id='documentation').findAll('dl')
        ]

    def find_videos(self, soup):
        return soup.find(id="video").findAll('iframe')


class ConcreteIsolux(ModelParseStrategy):
    domain = 'https://www.isolux.ru/'
    no_image_url = property(abc.abstractmethod(lambda _: _))
    response_router = {
        'description': '?description',
        'char': '?specification',
        'doc': '?documentation',
        'video': '?description',
    }

    def find_images(self, soup):
        return soup.find(id='product-summary-slider-main').findAll('img')

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        description = soup.find(id='product-description-inner')
        for video in self.find_videos(soup):
            description = description.replace(str(video), '')
        return description

    def find_chars(self, soup):
        return [
            (i.find('dt').text, i.find('dd').text)
            for i in soup.find(id='product-specification-inner').findAll('dl')
        ]

    def find_docs(self, soup):
        return [
            self.Doc(
                href=i.find('iframe')['src'],
                title=getattr(i.find('span'), 'text')
            )
            for i in soup.find(id='product-documentation-inner').findChildren('div', recursive=False)
            if i.find('iframe')
        ]

    def find_videos(self, soup):
        return soup.find(id='product-description-inner').findAll('iframe')


class ConcreteMircli(ModelParseStrategy):
    domain = 'https://mircli.ru/'

    def find_images(self, soup):
        return soup.find(class_='main_slider_product_fotorama').findAll('img')

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        return soup.find(
            id='product-overview-1'
        ).find(
            class_='block-product-overview__content'
        ).find(
            class_='show-more-block-new'
        )

    def find_chars(self, soup):
        chars = []
        try:
            chars += [
                (i.find(class_='main').text, i.find(class_='page').text)
                for i in soup.find(id='product-overview-2').findAll('li')
                if i.find(class_='main').text != 'Гарантия'
            ]
        except AttributeError:
            pass
        try:
            chars += [
                (i.find(class_='main').text, i.find(class_='page').text)
                for i in soup.find(class_='product-characters').findAll('li')
            ]
        except AttributeError:
            pass
        return chars

    def find_docs(self, soup):
        return []

    def find_videos(self, soup):
        video = soup.find(id='fotorama-product').find('a', {'data-video': 'true'})
        if not video:
            return []
        link = 'https://www.youtube.com/embed/{video_id}'.format(
            video_id=next(
                i.split('=')[1]
                for i in video['href'].split('?')[-1].split('&')
                if i.split('=')[0] == 'v'
            )
        )
        return [{'src': link}]


class ConcreteTerrem(ModelParseStrategy):
    domain = 'https://www.terrem.ru/'

    def find_images(self, soup):
        return [{'src': i['href']} for i in soup.find(class_='shop_prd_img').findAll(class_='shop_img')]

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        return soup.find('div', {'itemprop': 'description'})

    def find_chars(self, soup):
        return [
            (i.findAll('td')[0].text, i.findAll('td')[1].text)
            for i in soup.find(class_='shop_optn_tbl').findAll('tr')
        ]

    def find_docs(self, soup):
        return [
            self.Doc(
                href=i['href'],
                title=i['title']
            )
            for i in soup.find(class_='tab_container').findAll('a', class_='lightbox')
        ]

    def find_videos(self, soup):
        return soup.find(class_='tab_container').findAll('iframe')


class ConcreteRDStroy(ModelParseStrategy):
    domain = 'https://rdstroy.ru/'
    brand_phrase = '*Будем признательны, если Вы оставите свой отзыв о товаре ниже.'

    def find_images(self, soup):
        return [{'src': i['href']} for i in soup.findAll(class_='fancybox-image')]

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        return soup.find('div', {'itemprop': 'description'})

    def find_chars(self, soup):
        tr_list = soup.find(class_='ga-item-characteristics-list').findAll('tr')
        return [
            (i.findAll('td')[0].text, i.findAll('td')[1].text)
            for i in tr_list
            if 2 <= len(i.findAll('td'))
        ] + [
            (i.findAll('td')[2].text, i.findAll('td')[3].text)
            for i in tr_list
            if 4 <= len(i.findAll('td'))
        ]

    def find_docs(self, soup):
        return [
            self.Doc(
                title=i.find('a').find(text=True),
                href=i.find('a')['href']
            )
            for i in soup.findAll(class_='ga-item-certificate-item')
        ]

    def find_videos(self, soup):
        return soup.find('div', {'itemprop': 'description'}).findAll('iframe')


class ConcreteBCECMECU(ModelParseStrategy):
    domain = 'https://bce-cmecu.ru/'

    def find_images(self, soup):
        images = []
        for image in soup.find(class_='product-gallery').findAll('a'):
            try:
                int(image.get('data-uk-product-gallery-item', ''))
                images.append({'src': image['href']})
            except ValueError:
                pass
        return images

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        return soup.find(id='product-description')

    def find_chars(self, soup):
        """Характеристики у них прямо в описании"""
        return []

    def find_docs(self, soup):
        """Из документов нашел только ценовый группы"""
        return []

    def find_videos(self, soup):
        videos = []
        for video in soup.find(class_='product-gallery').findAll('a', {'data-uk-product-gallery-item': 'video'}):
            split = video['href'].split('/')
            video['href'] = '/'.join(split[:-1] + ['embed'] + split[-1:])
            videos.append(video)
        return videos


class ConcreteKuzmich(ModelParseStrategy):
    domain = 'https://kuzmich24.ru/'
    brand_phrase = 'Производитель оставляет за собой право изменять страну производства, характеристики товара, его внешний вид и комплектность без предварительного уведомления продавца. Уточняйте информацию у менеджеров!'

    def find_images(self, soup):
        return [
            {'src': i}
            for i in {i['href'] for i in soup.find(class_='item__slider').findAll('a')}
        ]

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        return soup.find('div', {'itemprop': 'description'})

    def find_chars(self, soup):
        chars = []
        try:
            num_tab = None
            for i, li in enumerate(soup.find('ul', {'class': 'tabs'}).findAll('li')):
                if li.text.lower() == u'характеристики':
                    num_tab = i
                    break
            if num_tab is None:
                return []
            chars += [
                (i.findAll('td')[0].text, i.findAll('td')[1].text)
                for i in soup.findAll(class_='tabs__content')[num_tab].findAll('tr')
            ]
        except AttributeError:
            pass
        try:
            chars += [
                    (i.find('span').text.rstrip(':'), i.find(class_='cell').text)
                for i in soup.find('div', {'itemprop': 'offers'}).find(class_='table').findAll('li')
            ]
        except AttributeError:
            pass
        return chars


    def find_docs(self, soup):
        return []

    def find_videos(self, soup):
        return []


class ConcreteGrandLine(ModelParseStrategy):
    domain = 'https://www.grandline.ru/'

    def find_images(self, soup):
        return [
            {'src': i['data-lazy'].replace('85x55', '500x335')}
            for i in soup.find(class_='product-switcher-holder').findAll('img')
        ]

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        description = soup.find(class_='description')
        self.brand_phrase = str(description.find(class_='p-view__links'))
        return description

    def find_chars(self, soup):
        return [
            (
                i.find(class_='specification-item__title').text,
                i.find(class_='specification-item__value').text
            )
            for i in soup.findAll(class_='full-specifications')[1].findAll(class_='specification-item')
        ]

    def find_docs(self, soup):
        return [
            self.Doc(
                href=i.find('a')['href'],
                title=i.find('a').text
            )
            for i in soup.find(class_='documents-list').findAll(class_='document-item')
        ]

    def find_videos(self, soup):
        return []


class Concrete24SD(ModelParseStrategy):
    domain = 'https://24sd.ru/'

    def find_images(self, soup):
        return [{'src': i['href']} for i in soup.find(class_='thumbnails').findAll('a')]

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        return soup.find(id='tab-description').find(class_='tab-bg')

    def find_chars(self, soup):
        chars = []
        for tbody in soup.find(id='tab-specification').findAll('tbody'):
            for i in tbody.findAll('tr'):
                chars.append((
                    i.findAll('td')[0].text,
                    i.findAll('td')[1].text
                ))
        return chars

    def find_docs(self, soup):
        return []

    def find_videos(self, soup):
        return []


class ConcreteStrugger(ModelParseStrategy):
    domain = 'https://strugger.ru/'

    def find_images(self, soup):
        """Картинки подгружаются динамически"""
        return [{'sr': i['data-image']} for i in soup.find(class_='image-additional').findAll('a')]

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        return soup.find(id='collapse-description')

    def find_chars(self, soup):
        return [
            (
                i.find(class_='propery-title').text,
                i.find(class_='propery-des').text
            )
            for i in soup.find(class_='product-property-list').findAll('li')
        ]

    def find_docs(self, soup):
        return []

    def find_videos(self, soup):
        return []


class ConcreteStriwer(ModelParseStrategy):
    domain = 'https://striwer.ru/'

    def find_images(self, soup):
        return [{'src': i['href']} for i in soup.find(class_='gallery').findAll('a')]

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        return soup.find(class_='editor-crop')

    def find_chars(self, soup):
        return [
            (
                i.findAll(class_='td')[0].text.split('?')[0],
                i.findAll(class_='td')[1].text
            )
            for i in soup.find(id='tabs-2').findAll(class_='tr')
        ]

    def find_docs(self, soup):
        return [
            self.Doc(
                href=i['data-src'].replace('https://drive.google.com/viewerng/viewer?embedded=true&url=', ''),
                title='Без Названия'
            )
            for i in soup.find(id='tabs-docs').findAll('iframe')
        ]

    def find_videos(self, soup):
        return [
            {'src': i['data-src']}
            for i in soup.find(id='tabs-video').findAll('iframe')
        ]


class ConcreteLCN(ModelParseStrategy):
    domain = 'https://lcn.ru/'

    def find_images(self, soup):
        return (
            [
                {'src': i['src'].replace('/xsmall_image/', '/large_image/')}
                for i in soup.find(class_='product--preview__nav-slider').findAll('img')
            ]
            or [
                {'src': soup.find(class_='product--preview__main-slider-item')['href']}
            ]
        )

    def find_unit(self, soup):
        return None

    def find_description(self, soup):
        description = soup.find(id='description').find('div')
        for i, child in enumerate(description.findChildren(recursive=False)):
            if 'ОСТАЛИСЬ ВОПРОСЫ? МЫ ВАМ ПОМОЖЕМ!' in child:
                self.brand_phrase = ''.join(map(str, description.findChildren(recursive=False)[i:]))
                break
        return description

    def find_chars(self, soup):
        return [
            (
                i.findAll('td')[0].text,
                i.findAll('td')[1].text
            )
            for i in soup.find(id='characteristics').find('tbody').findAll('tr')
            if i.findAll('td')
        ]

    def find_docs(self, soup):
        return []

    def find_videos(self, soup):
        return []


class ConcreteYaStroyMarket(ModelParseStrategy):
    domain = 'https://yastroymarket.ru/'

    def find_images(self, soup):
        return [
            {'src': i.get('data-large_image') or i.get('data-src') or i.get('src')}
            for i in soup.find(class_='product-images-wrapper').findAll('img')
        ]

    def find_unit(self, soup):
        pass

    def find_description(self, soup):
        return soup.find(id='tab-description').find(class_='electro-description')

    def find_chars(self, soup):
        return [
            (i.find('th').text, i.find('td').text)
            for i in soup.find(id='tab-specification').findAll('tr')
        ]

    def find_docs(self, soup):
        return []

    def find_videos(self, soup):
        return []


class ConcreteOboiDa(ModelParseStrategy):
    domain = 'https://oboida.ru/'

    def find_images(self, soup):
        return [
            {'src': i['href']}
            for i in soup.find(id='blueimp-gallery').parent.findAll('a')
            if i.get('data-gallery') is not None
        ]

    def find_unit(self, soup):
        pass

    def find_description(self, soup):
        return soup.find(id='opisanie')

    def find_chars(self, soup):
        return [
            (i.find('th').text, i.find('td').text)
            for i in soup.find('table', {'class': 'khar'}).findAll('tr')
        ]

    def find_docs(self, soup):
        return []

    def find_videos(self, soup):
        return []


class ConcretePetrovich(ModelParseStrategy):
    domain = 'https://petrovich.ru/'

    def find_images(self, soup):
        return [
            {'src': i['src'].replace('original-100x100', 'original-925x925')}
            for i in soup.find(class_='catalog-gallery-page-thumbs').findAll('img')
        ]

    def find_unit(self, soup):
        pass

    def find_description(self, soup):
        return soup.find(class_='product-details')

    def find_chars(self, soup):
        return [
            (i.find(class_='title').text, i.find(class_='value').text)
            for i in soup.find(class_='product-properties-list').findAll('li')
        ]

    def find_docs(self, soup):
        return [
            self.Doc(
                title=u'Сертификат',
                href=':'.join([self.get_http_protocol(), i['href']])
            )
            for i in soup.find(class_='product-block-previews').findAll('a')
        ]

    def find_videos(self, soup):
        return []


class ConcreteLeroymerlin(ModelParseStrategy):
    domain = 'https://leroymerlin.ru/'

    def __init__(self, model, *args, **kwargs):
        model.origin_product.other_shop = model.origin_product.other_shop.lower().replace('%2f', '/').replace('%3a', ':')
        super(ConcreteLeroymerlin, self).__init__(model, *args, **kwargs)

    def find_images(self, soup):
        return [
            {'src': i.find('source')['srcset']}
            for i in soup.findAll('picture', {'slot': 'pictures'})
        ]

    def find_unit(self, soup):
        pass

    def find_description(self, soup):
        return soup.find(id='nav-description')

    def find_chars(self, soup):
        return [
            (i.find(class_='def-list__term').text, i.find(class_='def-list__definition').text)
            for i in soup.find(id='nav-characteristics').findAll(class_='def-list__group')
        ]

    def find_docs(self, soup):
        return []

    def find_videos(self, soup):
        return []
