# coding=utf-8
import re
import pandas as pd
from progress.bar import Bar
from django.core.management.base import BaseCommand
from django_bulk_update.helper import bulk_update
from apps.models.models import ProductCharacteristic
from apps.models.models.characteristics import CharName, CharUnit, ProductChar, TokenField, CharToken


DEFAULT_VALUES_TO_DF = {
    'char_id': None,
    'charname': '',
    'model_id': None,
    'model_title': '',
    'old_char_id': None,
    'to_compare': ProductChar._meta.get_field('to_compare').default,
    'unit': None,
    'value': '',
}


class NormalizerConveyor:
    """Conveyor for normalize char"""
    SPECIAL_SYMBOL = r'[@_!#$%^&*()<>?/\|}{~: ,.]'
    BOOL_SYMBOL = '+'

    def __init__(self, df):
        self.conveyor = [
            self._drop_redundant_info,
            self._prepare_value,
            self._split_unit,
            self._clear_charname_from_unit,
            self._retokenization,
        ]
        self.global_conveyor = [
            self._pullout_char_from_title,
        ]
        self.df = df
        self.token_memory = {'unit': {}, 'charname': {}}
        self.unit_memory = None

    def _drop_redundant_info(self, row):
        row.charname = row.charname.split('\n')[0].strip()
        row.value = row.value.strip()
        return row

    def _prepare_value(self, row):
        row.value = row.value.lower()
        templates = {
            r'.*\d.*': (
                (u''.join(['(^|', self.SPECIAL_SYMBOL, ')от', self.SPECIAL_SYMBOL]), ''),
                (u''.join([self.SPECIAL_SYMBOL, 'до', self.SPECIAL_SYMBOL]), '-'),

                (u''.join(['(^|', self.SPECIAL_SYMBOL, ')не более', self.SPECIAL_SYMBOL]), '<='),
                (u''.join(['(^|', self.SPECIAL_SYMBOL, ')не менее', self.SPECIAL_SYMBOL]), '>='),
                (u''.join(['(^|', self.SPECIAL_SYMBOL, ')не выше', self.SPECIAL_SYMBOL]), '<='),
                (u''.join(['(^|', self.SPECIAL_SYMBOL, ')не ниже', self.SPECIAL_SYMBOL]), '>='),
                (u''.join(['(^|', self.SPECIAL_SYMBOL, ')больше', self.SPECIAL_SYMBOL]), '>'),
                (u''.join(['(^|', self.SPECIAL_SYMBOL, ')меньше', self.SPECIAL_SYMBOL]), '<'),
                (u''.join(['(^|', self.SPECIAL_SYMBOL, ')выше', self.SPECIAL_SYMBOL]), '>'),
                (u''.join(['(^|', self.SPECIAL_SYMBOL, ')ниже', self.SPECIAL_SYMBOL]), '<'),
                (u''.join(['(^|', self.SPECIAL_SYMBOL, ')равно', self.SPECIAL_SYMBOL]), '='),
            ),
            ur'(да|есть)': (
                (u'(да|есть)', self.BOOL_SYMBOL),
            ),
        }
        for template, rules in templates.iteritems():
            if not re.match(template, row.value):
                continue
            for rule in rules:
                row.value = re.sub(rule[0], rule[1], row.value)
        return row

    def _split_unit(self, row):
        if row.unit:
            return row
        parsed_out = re.findall(r'[\d., //\-±+><=]+|.+', row.value)
        row.value = parsed_out[0].strip()
        try:
            _unit = parsed_out[1].strip()
            if CharUnit._meta.get_field('unit').max_length < len(_unit):
                row.value = ' '.join([row.value, _unit])
                return row
            row.unit = _unit
        except IndexError:
            return row
        return row

    def _clear_charname_from_unit(self, row):
        if not self.unit_memory:
            self.unit_memory = [i.lower() for i in self.df[self.df.unit.notnull()].unit.unique()]
        for unit in self.unit_memory:
            row.charname = re.sub(
                r'(^|{symbol}+{unit}({symbol}|$)+)'.format(symbol=self.SPECIAL_SYMBOL, unit=unit),
                '',
                row.charname
            )
        return row

    def _retokenization(self, row):
        """re-tokenization is required even with an existing token"""
        row.unit_token = None
        if row.unit:
            unit_token = self.token_memory['unit'].get(row.unit)
            if not unit_token:
                unit_token = TokenField.generate(row.unit)
                self.token_memory['unit'][row.unit] = unit_token
            row['unit_token'] = unit_token
        char_token = self.token_memory['charname'].get(row.charname)
        if not char_token:
            char_token = TokenField.generate(row.charname)
            self.token_memory['charname'][row.unit] = char_token
        row['char_group'] = char_token
        return row

    def _pullout_char_from_title(self):
        boolean_chars = self.df[self.df.value == '+'].charname.unique()
        for char in boolean_chars:
            models_with_char = self.df[(self.df.model_title.str.contains(char)) & (self.df.charname != char)]
            models_with_char = models_with_char.model_id.unique()
            VALUES_TO_DF = {'charname': char, 'value': self.BOOL_SYMBOL}
            self.df = self.df.append(pd.DataFrame([
                {
                    column: (
                        VALUES_TO_DF.get(column, None) or DEFAULT_VALUES_TO_DF[column]
                        if column != 'model_id'
                        else model_id
                    )
                    for column in self.df.columns
                }
                for model_id in models_with_char
            ]))

    def loop(self):
        with Bar('Conveyor: ', max=len(self.conveyor)) as bar:
            for func in bar.iter(self.conveyor):
                self.df = self.df.apply(func, axis=1)
        with Bar('Global Conveyor: ', max=len(self.global_conveyor)) as bar:
            for func in bar.iter(self.global_conveyor):
                func()
        del self.token_memory
        del self.unit_memory

    def sync2db(self):
        print 'Update units...'
        unit_dict = {i.token: i.id for i in CharUnit.objects.only('token')}
        units2create = {
            row.unit_token: CharUnit(token=row.unit_token, unit=row.unit)
            for i, row in self.df[self.df.unit_token.notnull() & ~self.df.unit_token.isin(unit_dict)].iterrows()
        }
        CharUnit.objects.bulk_create(units2create.values())
        unit_dict.update({
            i.token: i.id
            for i in CharUnit.objects.filter(token__in=units2create.keys()).only('token')
        })
        print 'Update char tokens...'
        group_dict = {i.token: i.id for i in CharToken.objects.only('token')}
        groups2create = {
            row.char_group: CharToken(token=row.char_group)
            for i, row in self.df[self.df.char_group.notnull() & ~self.df.char_group.isin(group_dict)].iterrows()
        }
        CharToken.objects.bulk_create(groups2create.values())
        group_dict.update({
            i.token: i.id
            for i in CharToken.objects.filter(token__in=groups2create.keys()).only('token')
        })
        print 'Update char names...'
        name_df = self.df[self.df.char_id.isnull()].drop_duplicates(subset=['charname'])
        existed_names = CharName.objects.filter(name__in=[i for i in name_df.charname]).values_list('name', flat=True)
        CharName.objects.bulk_create([
            CharName(name=row.charname, group_id=group_dict.get(row.char_group))
            for i, row in name_df.iterrows()
            if row.charname not in existed_names
        ])

        print 'Create new chars...'
        ProductChar.objects.bulk_create([
            ProductChar(
                product_id=row.model_id,
                char_id=group_dict.get(row.char_group),
                value=row.value,
                unit_id=unit_dict.get(row.unit_token),
                to_compare=row.to_compare,
                old_char_id=row.old_char_id,
            )
            for i, row in self.df[self.df.char_id.isnull()].iterrows()
            if len(row.value) <= ProductChar._meta.get_field('value').max_length
        ])
        print 'Update old chars...'
        bulk_update(
            [
                ProductChar(
                    id=row.char_id,
                    product_id=row.model_id,
                    char_id=group_dict.get(row.char_group),
                    value=row.value,
                    unit_id=unit_dict.get(row.unit_token),
                    to_compare=row.to_compare,
                    old_char_id=row.old_char_id,
                )
                for i, row in self.df[self.df.char_id.notnull()].iterrows()
            ],
            update_fields=['product_id', 'char_id', 'value', 'unit_id', 'to_compare', 'old_char_id']
        )


class Command(BaseCommand):
    """To test the hypothesis <DEV-585>"""
    args = '<section_id>'
    help = 'Migrate old chars to new style into database'

    def handle(self, *args, **options):
        print 'Query to chars...'
        df = pd.concat(
            [
                pd.DataFrame([
                    {
                        'charname': char.characteristic.group_name,
                        'value': char.characteristic.value,
                        'unit': DEFAULT_VALUES_TO_DF['unit'],
                        'model_id': char.product_model_id,
                        'model_title': char.product_model.title,
                        'char_id': DEFAULT_VALUES_TO_DF['char_id'],
                        'old_char_id': char.id,
                        'to_compare': char.to_compare,
                    }
                    for char in ProductCharacteristic.objects.filter(
                        productchar=None,
                        product_model__section__in=args
                    ).select_related('characteristic').prefetch_related('product_model')
                ]),
                pd.DataFrame([
                    {
                        'charname': char.char.name,
                        'value': char.value,
                        'unit': getattr(char.unit, 'unit', DEFAULT_VALUES_TO_DF['unit']),
                        'model_id': char.product_id,
                        'model_title': char.product.title,
                        'char_id': char.id,
                        'old_char_id': char.old_char_id,
                        'to_compare': char.to_compare,
                    }
                    for char in ProductChar.objects.filter(
                        product__section__in=args
                    ).prefetch_related('char__charname_set', 'product')
                ]),
            ],
            ignore_index=True
        )
        fsm = NormalizerConveyor(df)
        fsm.loop()
        fsm.sync2db()
