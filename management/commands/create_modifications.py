# coding=utf-8
import os
import pandas as pd
import tempfile
from progress.bar import Bar
from optparse import make_option
from sklearn.cluster import DBSCAN
from sklearn.feature_extraction.text import TfidfVectorizer
from django.core.management.base import BaseCommand
from django_bulk_update.helper import bulk_update
from common.utils.elastic_prepare import normalize_phrase
from apps.models.models import Product, ProductModel
from apps.parser import ParseController


class Command(BaseCommand):
    args = '<company_id>'
    help = 'Create modifications using clustering'
    option_list = BaseCommand.option_list + (
        make_option(
            '--save',
            action='store_true',
            dest='save',
            default=False,
            help='Save models to database'
        ),
        make_option(
            '--parse',
            action='store_true',
            dest='parse',
            default=False,
            help='Parse created models'
        ),
        make_option(
            '--report',
            action='store_true',
            dest='report',
            default=False,
            help='Create report file'
        ),
        make_option(
            '--start',
            action='store',
            type='int',
            dest='start',
            default=0,
            help='Start script with {n} section'
        ),
        make_option(
            '--origin-company',
            action='store',
            type='int',
            dest='origin_company',
            default=0,
            help='Set to new models this company'
        ),
        make_option(
            '--section',
            action='store',
            type='int',
            dest='section',
            default=0,
            help='Start script with section'
        ),
    )
    SYNTHETIC_DF_COLUMNS = ['model_id', 'model_title', 'model_is_new', 'product_id', 'product_title']
    FILENAME = os.path.join(tempfile.gettempdir(), 'created_models.csv')

    def handle(self, *args, **options):
        from apps.elasticsearch.models import UpdatedItem
        company_products = set()
        section_ids = set()
        print 'Prepare Companies...'
        products = Product.objects.filter(company_id__in=args).only('section')
        if options['section']:
            products = products.filter(section_id=options['section'])
        for product in products:
            company_products.add(product.id)
            section_ids.add(product.section_id)

        if options['report']:
            pd.DataFrame(
                columns=self.SYNTHETIC_DF_COLUMNS
            ).to_csv(self.FILENAME)

        print 'Query to ProductModel...'
        # Нужна для поиска дубликатов
        correlation_origin_product = {
            i.origin_product_id: i.id
            for i in ProductModel.objects.only('origin_product')
        }

        for i, section_id in enumerate(section_ids):
            print 'Section: {i}/{count}, ({section_id})'.format(i=i + 1, count=len(section_ids), section_id=section_id)
            if i + 1 < options['start']:
                continue
            print 'Query to Product...'
            df = pd.DataFrame(
                Product.objects.filter(
                    section_id=section_id
                ).values('id', 'title', 'product_model', 'list_count')
            )
            print 'Prepare data...'
            df['prepared_title'] = df.apply(
                lambda row: ' '.join(normalize_phrase(row['title'], with_delay=True)),
                axis=1
            )
            del df['title']
            print 'Clustering...'
            X = TfidfVectorizer(
                binary=True,
                norm=None,
                use_idf=False,
            ).fit_transform(df['prepared_title'])
            df['cluster'] = DBSCAN(eps=0.575, min_samples=1).fit_predict(X)
            # Вырежем шумы
            df = df[df['cluster'] != -1]
            # Разделим существующие модели и новые
            existed_models = df[df['product_model'].notnull()]
            df = df[df['product_model'].isnull()]
            # Группируем данные
            if len(existed_models):
                existed_models = existed_models.groupby(
                    ['cluster', 'prepared_title']
                )['product_model'].apply(max).reset_index().set_index('prepared_title')
            data = df.fillna({'list_count': 0}).sort_values(['list_count']).set_index('id')
            max_ctr = data['list_count'].max()
            df = df.groupby(['cluster', 'prepared_title'])['id'].apply(
                lambda series: sorted(  # сортируем обьект Series по CTR
                    list(series),
                    key=lambda i: data.loc[i]['list_count'] + (max_ctr if i in company_products else 0),
                    reverse=True
                )
            ).reset_index()
            # оставим только кластеры с продуктами из существующей компании
            df = df[df.apply(lambda x: bool(set(x['id']) & company_products), axis=1)]
            synthetic_df = pd.DataFrame(
                columns=self.SYNTHETIC_DF_COLUMNS
            )
            print 'Query to Product...'
            # sum списков даст конкатенцию
            products = {
                i.id: i
                for i in Product.objects.filter(id__in=df['id'].sum() or [])
            }
            to_update_products = []
            models2parse = []
            if not len(df):
                continue
            with Bar('Create models:', max=len(df), suffix='%(percent)d%%') as bar:
                for index, row in bar.iter(df.iterrows()):
                    model = None
                    most_popular_product = products[row['id'][0]]
                    is_new = True
                    try:
                        model_id = (
                            correlation_origin_product.get(most_popular_product.id, None)
                            or existed_models.loc[row['prepared_title']]['product_model']
                        )
                        is_new = False
                    except KeyError:
                        model = ProductModel(
                            title=most_popular_product.title,
                            origin_product=most_popular_product,
                            section_id=section_id,
                            origin_company_id=options['origin_company'] or most_popular_product.company_id,
                            description=most_popular_product.description or '',
                            description_tag=most_popular_product.description_tag,
                            brand_id=most_popular_product.brand_id,
                            active=False,
                        )
                        if options['save']:
                            model.save()
                            models2parse.append(model)
                        model_id = model.id

                    for product_id in row['id']:
                        product = products[product_id]
                        product.product_model_id = model_id
                        to_update_products.append(UpdatedItem(item_type='product', item_id=product.id))
                        synthetic_df = synthetic_df.append(
                            {
                                'model_id': model_id,
                                'model_title': model.title if model else '',
                                'model_is_new': is_new,
                                'product_id': product.id,
                                'product_title': product.title,
                            },
                            ignore_index=True
                        )

            if options['save']:
                print 'Products bulk save...'
                bulk_update(products.values(), update_fields=['product_model'])
                UpdatedItem.objects.bulk_create(to_update_products)

                if options['parse']:
                    bulk_parser = ParseController(models2parse, with_bar=True)
                    bulk_parser.parse()
                    bulk_parser.clear()
            if options['report']:
                print 'File reported...'
                synthetic_df.to_csv(self.FILENAME, mode='a', header=False)
